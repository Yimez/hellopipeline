terraform {
  backend "s3" {
    bucket = "stagebucketgil"
    key    = "azure/terraform.tfstate"
    region = "eu-west-3"
    encrypt = true
  }
}