variable "name" {
  default = "string"
}
variable "location" {
  default = "string"
}

variable "sku_name" {
  default = "string"
}

variable "db_version" {
  default = "5.7"
}

variable "db_name" {
  default = "string"
}

