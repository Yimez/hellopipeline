resource "azurerm_mysql_server" "mysql" {
    name = var.name
    location = var.location
    resource_group_name = "gilRG"
    
    administrator_login = "pxl"
    administrator_login_password = "P@ssword123"
    
    sku_name =  var.sku_name
    storage_mb = 5120
    version = var.db_version
    ssl_enforcement_enabled = false
}
resource "azurerm_mysql_database" "example" {
  name                = var.db_name
  resource_group_name = "gilRG"
  server_name         = azurerm_mysql_server.mysql.name
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}