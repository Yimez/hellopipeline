provider "azurerm" {
  features {}
  //version                    = "=2.0.0"
  skip_provider_registration = true
}



module "linuxservers" {
  #Demo comment
  #Required
  source              = "Azure/compute/azurerm"
  resource_group_name = "gilRG"
  vnet_subnet_id      = module.network.vnet_subnets[0]
  #Optional
  vm_os_simple  = "CentOS"
  public_ip_dns = ["nfusegiltest"]
  vm_hostname   = "nfusewebserverazure"
}
module "network" {
  source              = "Azure/network/azurerm"
  resource_group_name = "gilRG"
  address_space       = "10.0.0.0/16"
  subnet_prefixes     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  subnet_names        = ["subnet1", "subnet2", "subnet3"]
}

module "network-security-group" {
  source                = "Azure/network-security-group/azurerm"
  resource_group_name   = "gilRG"
  security_group_name   = "nsg"
  source_address_prefix = ["10.0.1.0/24"]

  custom_rules = [
    {
      name                   = "ssh"
      priority               = "200"
      direction              = "Inbound"
      access                 = "Allow"
      protocol               = "tcp"
      destination_port_range = "22"
      description            = "ssh"
    },
    {
      name                   = "http"
      priority               = "201"
      direction              = "Inbound"
      access                 = "Allow"
      protocol               = "tcp"
      destination_port_range = "80"
      description            = "http"
    }
  ]

}
module "mysql" {
  source = ".//modules/mysqldb"
  name = "nfusesqlDB"
  location = "westeurope"
  sku_name = "B_Gen4_2"
  db_name = "nfuseDatabase"

}

data "template_file" "ansible_inventory" {
  template = "${file("/home/ubuntu/ansible/azure/hosts.tmpl")}"

  vars = {
    public_ips = "${join("\n", module.linuxservers.public_ip_address)}"
    endpoint = "${join("\n", list(module.mysql.fqdn))}"
  }
}

resource "local_file" "hosts" {
  filename = "/home/ubuntu/ansible/azure/hosts"

  content = data.template_file.ansible_inventory.rendered
}
