provider "aws" {
  region = "eu-west-3"
}
/*
module "vpc" {
  source = ".//modules/vpc"
}
*/

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["eu-west-3a"]
  private_subnets = ["10.0.1.0/24"]
  public_subnets  = ["10.0.101.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
module "security-group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "3.4.0"
  name    = "sgWeb"
  vpc_id  = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

  egress_with_cidr_blocks = [{
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "0.0.0.0/0"
  }]

}

module "security-group-db" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "3.4.0"
  name    = "sgRds"
  vpc_id  = module.vpc.vpc_id

  ingress_with_self = [
    {
      rule = "all-all"
    },
    {
      from_port = 3306
      to_port   = 3306
      protocol  = "tcp"
    },
    {
      from_port = 22
      to_port   = 22
      protocol  = "tcp"
    }
  ]
}

module "ec2" {
  #EC2
  source            = ".//modules/ec2"
  ami_id            = "ami-6276c71f"
  instance_type     = "t2.medium"
  availability_zone = "eu-west-3a"
  vm_tag_name       = "Terraform-Web"
  key_name          = "stageKey"
  sec_group         = [module.security-group.this_security_group_id]
  subnet_id         = module.vpc.public_subnets[0]
}

module "rds" {
  source = ".//modules/rds"

  #RDS
  allocated_storage = "10"
  storage_type      = "gp2"
  engine            = "mysql"
  engine_version    = "5.7.26"
  instance_class    = "db.t2.micro"
  db_name           = "Terraformdb"
  db_uname          = "pxl"
  db_pw             = "pxlpxlpxl"
  identifier        = "nfusetestdb"
  pub_acces         = true

}

data "template_file" "ansible_inventory" {
  template = "${file("/home/ubuntu/ansible/azure/hosts.tmpl")}"

  vars = {
    public_ips = "${join("\n", module.ec2.public_ip)}"
    endpoint = "${join("\n", list(module.rds.endpoint))}"
  }
}

resource "local_file" "hosts" {
  filename = "/home/ubuntu/ansible/aws/hosts"

  content = data.template_file.ansible_inventory.rendered
}
