variable "cidr_block" {
  default = "string"
}
variable "tenancy" {
  default = "string"
}
variable "network_name" {
  default = "string"
}

variable "vpc_id" {
  default = "string"
}
variable "subnet_ids" {
  default= "string"
}

variable "vpc_cidr" {
  default = "string"
}

variable "subnet_cidr" {
  default ="string"
}


