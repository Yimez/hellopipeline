#Instance ID
variable "ami_id" {
  default = "string"
}

variable "instance_type" {
  default = "string"

}

variable "vpc_security_group_ids" {
  default = "list"
}
variable "sec_group" {
  default = "list"
  
}

variable "key_name" {
  default = "string"

}

variable "availability_zone" {
  default = "string"
}

variable "subnet_id" {
  default = "string"
}

variable "vm_tag_name" {
  default = "string"
}








