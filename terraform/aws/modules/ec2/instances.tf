resource "aws_instance" "web" {
  ami           = var.ami_id
  instance_type = var.instance_type
  //vpc_security_group_ids = var.vpc_security_group_ids
  key_name          = var.key_name
  availability_zone = var.availability_zone
  subnet_id              = var.subnet_id
  vpc_security_group_ids = var.sec_group



  tags = {
    Name = var.vm_tag_name
  }
}
