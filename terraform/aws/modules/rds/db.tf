resource "aws_db_instance" "rds" {
  allocated_storage = var.allocated_storage
  storage_type      = var.storage_type
  engine            = var.engine
  engine_version    = var.engine_version
  instance_class    = var.instance_class
  name              = var.db_name
  username          = var.db_uname
  password          = var.db_pw
  # password                    = aws_secretsmanager_secret_version.rdstf.secret_string
  identifier                = var.identifier
  publicly_accessible       = var.pub_acces
  final_snapshot_identifier = false
  skip_final_snapshot       = true

}
