#DB
variable "allocated_storage" {

  default = "string"
}

variable "storage_type" {
  default = "string"
}
variable "engine" {
  default = "string"
}
variable "engine_version" {
  default = "string"
}
variable "instance_class" {
  default = "string"
}
variable "db_name" {
  default = "string"
}
variable "db_uname" {
  default = "string"
}

variable "db_pw" {
  default ="string"
}
variable "identifier" {
   default = "string"
}
variable "pub_acces" {
  default = "bool"
}
