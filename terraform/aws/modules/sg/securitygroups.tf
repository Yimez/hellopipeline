#Default Security group for web servers.
resource "aws_security_group" "webSG" {
  name        = "WebSG"
  description = "Security group for webserver. Created by Terraform"
  vpc_id      = aws_vpc.myVpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "Allow SSH traffic"
  }

  /*  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    description = "Allow htpp traffic"
  }
  ingress {
    from_port = 443
    to_port   = 433
    protocol  = "tcp"
    description = "Allow Https traffic"
  }



  ingress {
      from_port = 9000
      to_port = 9000
      protocol = "tcp"
      description "Allow connection with SonarQube"
  }

  ingress{
      from_port = 3306
      to_port = 3306
      protocol = "tcp"
      description = "Allow connection with RDS DB"
  } */
}

resource "aws_security_group" "dbSG" {
  name        = "DB-terraform"
  description = "DB"

  ingress {
    from_port = 3306
    to_port   = 3306
    protocol  = "tcp"
    //security_groups = ["${aws_security_group.web.id}"]
  }

  tags = {
    Name = "DB-terraform"
  }
}

