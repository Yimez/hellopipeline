/*
output "myvpc"{
    value = module.vpc.vpc_id
}
*/
output "public_ip"{
    value = module.ec2.public_ip
}

output "endpoint"{
    value = module.rds.endpoint
}