module "vpc" {
  source  = "terraform-google-modules/network/google//modules/vpc"
  version = "2.3.0"

  project_id              = "gilstage"
  network_name            = "testvpc"
  auto_create_subnetworks = true

}


module "vpc-firewall" {
  source     = "terraform-google-modules/network/google//modules/fabric-net-firewall"
  project_id = "gilstage"
  network    = module.vpc.network_name

  custom_rules = {
    ingress-sample = {
      description          = "Dummy sample ingress rule, tag-based."
      direction            = "INGRESS"
      action               = "allow"
      ranges               = ["0.0.0.0/0"]
      sources              = ["web"]
      targets              = ["web"]
      use_service_accounts = false
      rules = [
        {
          protocol = "tcp"
          ports    = ["22", "80", "443", "3306", "9000"]
        }
      ]
      extra_attributes = {}
    }
  }
}


resource "google_compute_instance" "webserver" {
  project      = "gilstage"
  name         = "gcpweb"
  machine_type = "n1-standard-1"
  zone         = "europe-west6-a"
  tags         = ["web"]

  metadata = {
    ssh_keys = "~/.ssh/id_rsa.pub"
  }

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
    }
  }
  network_interface {
    network = module.vpc.network_name
    access_config {
      // Ephemeral IP
    }
  }
}


 module "sql-db_mysql" {
  source           = "GoogleCloudPlatform/sql-db/google//modules/mysql"
  version          = "3.2.0"
  database_version = "MYSQL_5_7"
  name             = "gilnfusegcpdb"
  project_id       = "gilstage"
  zone             = "a"
  region           = "europe-west6"
  user_name = "gil"
  user_password = "P@ssword123"
}
